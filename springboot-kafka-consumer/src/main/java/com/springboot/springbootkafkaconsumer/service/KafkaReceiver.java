package com.springboot.springbootkafkaconsumer.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.springboot.model.Student;

@Service
public class KafkaReceiver {

	private static final Logger LOGGER = LoggerFactory.getLogger(KafkaReceiver.class);

	/* Method to receive message from Topic1 */
	@KafkaListener(topics = "${kafka.topic.name1}", groupId = "${kafka.consumer.group.id}")
	public void receiveTopic1Data(Student student) {
		LOGGER.info("Topic 1 Data - " + student.toString() + " received");
		LOGGER.info("Data - Id:" + student.getId() + " , Name:" + student.getName() + " , Salary:" + student.getSalary());
	}
	
	/* Method to receive message from Topic2 */
	@KafkaListener(topics = "${kafka.topic.name2}", groupId = "${kafka.consumer.group.id}")
	public void receiveTopic2Data(Student student) {
		LOGGER.info("Topic 2 Data - " + student.toString() + " received");
		LOGGER.info("Data - Id:" + student.getId() + " , Name:" + student.getName() + " , Salary:" + student.getSalary());
	}
}