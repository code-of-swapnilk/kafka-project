Spring Boot with Kafka Producer-Consumer Example
This Project covers how to use Spring Boot with Spring Kafka to Publish and Subscribe JSON message to a Kafka topic

Start Zookeeper in Windows
Kafka/bin/window/->zookeeper-server-start.bat ../../config/zookeeper.properties

Start Kafka Server in Windows
Kafka/bin/window/->kafka-server-start.bat ../../config/server.properties