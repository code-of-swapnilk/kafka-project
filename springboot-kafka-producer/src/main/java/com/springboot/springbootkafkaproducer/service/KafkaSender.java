package com.springboot.springbootkafkaproducer.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Service;

import com.springboot.model.Student;

@Service
public class KafkaSender {

	public static final Logger LOGGER = LoggerFactory.getLogger(KafkaSender.class);
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	@Value("${kafka.topic.name1}")
	private String topicName1;
	
	@Value("${kafka.topic.name2}")
	private String topicName2;
	
	/* Method to send message to particular topic */
	public void sendData(Student student, String topicName) {
		Map<String, Object> headers = new HashMap<>();
		headers.put(KafkaHeaders.TOPIC, topicName);
		kafkaTemplate.send(new GenericMessage<Student>(student, headers));
		LOGGER.info("Data - " + student.toString() + " sent to Kafka Topic - " + topicName);
	}

	/* Method to send message in Topic1 */
	public void sendToTopic1(Student student) {
		sendData(student, topicName1);
	}
	
	/* Method to send message in Topic2 */
	public void sendToTopic2(Student student) {
		sendData(student, topicName2);
	}
}
