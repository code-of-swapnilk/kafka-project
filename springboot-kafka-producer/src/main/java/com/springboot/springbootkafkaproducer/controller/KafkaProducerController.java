package com.springboot.springbootkafkaproducer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.model.Student;
import com.springboot.springbootkafkaproducer.service.KafkaSender;

@RestController
@RequestMapping("/kafkaProducer")
public class KafkaProducerController {

	@Autowired
	private KafkaSender sender;
	
	@PostMapping("/topic1")
	public ResponseEntity<String> sendToTopic1(@RequestBody Student student){
		sender.sendToTopic1(student);
		return new ResponseEntity<>("Data sent to Kafka Topic 1 ", HttpStatus.OK);
	}
	
	@PostMapping("/topic2")
	public ResponseEntity<String> sendToTopic2(@RequestBody Student student){
		sender.sendToTopic2(student);
		return new ResponseEntity<>("Data sent to Kafka Topic 2 ", HttpStatus.OK);
	}
}
